import React from 'react';
import './App.css';
import 'antd/dist/antd.css'
import BookStore from "./component/BookStore";

function App() {
    return (
        <div className="App">
            <BookStore/>
        </div>
    );
}

export default App;
