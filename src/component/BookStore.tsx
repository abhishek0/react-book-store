import Book, {IBook} from "./Book";
import Cart from "./Cart";
import Filter from "./Filter";
import React, {useState, useEffect} from "react";
import {Button, Spin} from "antd";
import BookService from "../service/book-service";

const BookStore = () => {

    const [books, setBooks] = useState([] as IBook[]); // to store books details fetched from the api
    const [filterData, setFilterData] = useState({
        minPrice:1,
        maxPrice:999,
        minQty:0,
        page:1,
        calledFromFilter: false
    });
    const [totalAmount, setTotalAmount] = useState(0);
    const [isSpinning, setSpinning] = useState(true);
    const [noMoreData, setNoMoreData] = useState(false);
    const [totalOrderQuantity, setTotalOrderQuantity] = useState(0);

    useEffect(() => {
        (async () => {
            let booksData: IBook[];
            try {
                booksData = await BookService.fetchBooks(filterData);
            } catch (err) {
                console.log('err>>>>>>>>>>', err);
                booksData = [];
            }

            setNoMoreData(!booksData.length);

            filterData.calledFromFilter ?
                setBooks(booksData):
                setBooks(prevBooks => [...prevBooks, ...booksData]);

            setSpinning(false);
        })();
    }, [filterData]);

    const loadMoreData = () => {
        if(noMoreData) {
            console.log('No more data!!!!!!!!!!!!!!!!!');
        }
        const pageNo = filterData.page;

        setSpinning(true);
        setFilterData({
            ...filterData,
            page: pageNo+1,
            calledFromFilter: false
        });
    };

    return <Spin spinning={isSpinning}>
        <Filter
            setFilterData={setFilterData}
            setSpinning={setSpinning}
        />
        {
            books.map((book) => {
                return (
                    <React.Fragment key={String(book.id)}>
                    <Book
                        book={book}
                        setTotalAmount={setTotalAmount}
                        setTotalOrderQuantity={setTotalOrderQuantity}
                    />
                    </React.Fragment>
                )
            })
        }
        <Button
            type={'primary'}
            disabled={noMoreData}
            onClick={loadMoreData}>
            Load More
        </Button>
        <Cart
            totalAmount={totalAmount}
            totalOrderQuantity={totalOrderQuantity}
        />
    </Spin>
}

export default BookStore;
