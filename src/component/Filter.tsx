import {Button, Card, Form, Input} from "antd";

interface IProps {
    setFilterData: any;
    setSpinning: any;
}

const Filter = ({setFilterData, setSpinning}: IProps) => {
    const [form] = Form.useForm();

    const onFinish = (values: any) => {
        // deleting undefined form values
        Object.keys(values).forEach(key => {
            if(!values[key]) delete values[key];
            else values[key] = parseInt(values[key], 10);
        });

        setSpinning(true);
        setFilterData((prevData: any) => {
            return {
                ...prevData,
                ...values,
                calledFromFilter: true,
                page: 1
            };
        });
    };
    return <Card>
      <Form form={form} name="horizontal_login" layout="inline" onFinish={onFinish}>
        <Form.Item
          name="minPrice"
          // rules={[{ required: true, message: 'Please input your username!' }]}
        >
          <Input
              type="number"
              placeholder="min price"
          />
        </Form.Item>
        <Form.Item
          name="maxPrice"
        >
          <Input
            // prefix={<LockOutlined className="site-form-item-icon" />}
            type="number"
            placeholder="max price"
          />
        </Form.Item>
        <Form.Item
          name="minQty"
        >
          <Input
            type="number"
            placeholder="min quantity"
          />
        </Form.Item>
        <Form.Item>
            <Button
              type="primary"
              htmlType="submit"
              disabled={false}
            >
              Filter
            </Button>
        </Form.Item>
      </Form>
    </Card>
}

export default Filter;
