import {Card, Col, Row, Statistic} from "antd";

interface IProps {
    totalOrderQuantity: number;
    totalAmount: number;
}

const Cart = ({totalOrderQuantity, totalAmount}: IProps) => {
    const discountedAmount = () => {
        return (totalOrderQuantity > 10 || totalAmount > 1000) ?
            totalAmount-totalAmount*0.1:
            totalAmount;
    }
    return (
        <Card>
            <Row gutter={16}>
                <Col span={8}>
                    <Statistic title="Total Books to Order" value={`${totalOrderQuantity} Pc`} />
                </Col>
                <Col span={8}>
                    <Statistic
                        title="Total Discount"
                        value={`${parseInt(String(totalAmount-discountedAmount()), 10)} INR`}
                    />
                </Col>
                <Col span={8}>
                    <Statistic title="Total Amount" value={`${discountedAmount()} INR`} />
                </Col>
            </Row>
        </Card>
    )
}
export default Cart;
