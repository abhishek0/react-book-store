import {Card, InputNumber, Row, Col} from "antd";

import { PlusOutlined, MinusOutlined } from '@ant-design/icons';
import {FC, useState} from "react";

export interface IBook{
    id: number;
    title: string;
    description: string;
    totalQuantity: number;
    pricePerQty: number
}
interface IProps {
    book?: IBook;
    setTotalAmount: any;
    setTotalOrderQuantity: any;
}

const Book: FC<IProps> = ({
    book={},
    setTotalAmount,
    setTotalOrderQuantity
}) => {
    const {
        id=1212,
        title = "Book Name",
        description = "This is really nice book",
        totalQuantity = 10,
        pricePerQty = 135
    } = book;

    const [orderQuantity, setOrderQuantity] = useState(0);

    const incrementBookCount = () => {
        if(orderQuantity === totalQuantity) return;
        setOrderQuantity(+orderQuantity + 1);
        setTotalOrderQuantity((prevQuantity: number) => prevQuantity+1);
        setTotalAmount((prevAmount: number) => prevAmount+(book?.pricePerQty || 0));
    };

    const decrementBookCount = () => {
        if(orderQuantity === 0) return;
        setOrderQuantity(+orderQuantity - 1);
        setTotalOrderQuantity((prevQuantity: number) => prevQuantity-1);
        setTotalAmount((prevAmount: number) => prevAmount-(book?.pricePerQty || 0));
    };

    return <>
        <Card
            style={{ width: '80%', margin: '20px' }}
            actions={[
                    <PlusOutlined onClick={incrementBookCount} />,
                    <InputNumber disabled={true} value={orderQuantity} />,
                    <MinusOutlined onClick={decrementBookCount} />
            ]}
        >
            <Row>
                <Col span={4}>ID: {id}</Col>
                <Col span={8}>Name: {title}</Col>
                <Col span={12}>Description: {description}</Col>
            </Row>
            <Row>
                <Col span={12}>Total Quantity: {totalQuantity} Pc</Col>
                <Col span={12}>Price per Qty: INR {pricePerQty}</Col>
            </Row>

            <Row>
                <Col span={12}>Order Quantity: {orderQuantity} Pc</Col>
                <Col span={12}>Total Price: INR {orderQuantity*pricePerQty}</Col>
            </Row>
        </Card>
    </>
}
export default Book;
